<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Birthday Invitation</title>
<link rel="icon" type="image/png" href="images/favicon.png">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
<style>
    html, body {
      margin: 0;
      width: 100%;
      height: 100%;
	  font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
	
	*{
		box-sizing:border-box;	
	}
	
	.footer_rsvp{
		position:fixed;
		bottom:0px;
		width:100%;
		padding:10px;
		color:#FFF;
		background:#D01DBE;
		z-index:1000;
	}
	
	.header_direction{
		position:fixed;
		top:0px;
		width:100%;
		padding:10px;
		color:#FFF;
		background:#D01DBE;
		z-index:1000;
	}
	
	.direction_btn{
		float:right;
		background:#FFF;
		font-size:14px;
		border:0px;	
	}
	
	.confirm_btn{
		background:#FFF;
		font-size:14px;
		border:0px;
	}
	
	.rsvp_form{
		display:none;
		}
		
  </style>
  
<style type="text/css">
.form-style-2{
    max-width: 500px;
    padding: 20px 12px 10px 20px;
    font: 13px Arial, Helvetica, sans-serif;
}
.form-style-2-heading{
    font-weight: bold;
    font-style: italic;
    border-bottom: 2px solid #ddd;
    margin-bottom: 20px;
    font-size: 15px;
    padding-bottom: 3px;
}
.form-style-2 label{
    display: block;
    margin: 0px 0px 15px 0px;
}
.form-style-2 label > span{
    width: 100px;
    font-weight: bold;
    float: left;
    padding-top: 8px;
    padding-right: 5px;
}
.form-style-2 span.required{
    color:red;
}
.form-style-2 .tel-number-field{
    width: 40px;
    text-align: center;
}
.form-style-2 input.input-field{
    width: 48%;
   
}

.form-style-2 input.input-field_no{
	width: 10%;
	}

.form-style-2 input.input-field,
.form-style-2 input.input-field_no,
.form-style-2 .tel-number-field,
.form-style-2 .textarea-field,
 .form-style-2 .select-field{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    border: 1px solid #C2C2C2;
    box-shadow: 1px 1px 4px #EBEBEB;
    -moz-box-shadow: 1px 1px 4px #EBEBEB;
    -webkit-box-shadow: 1px 1px 4px #EBEBEB;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    padding: 7px;
    outline: none;
}
.form-style-2 .input-field:focus,
.form-style-2 input.input-field_no:focus,
.form-style-2 .tel-number-field:focus,
.form-style-2 .textarea-field:focus,  
.form-style-2 .select-field:focus{
    border: 1px solid #0C0;
}
.form-style-2 .textarea-field{
    height:100px;
    width: 55%;
}
.form-style-2 input[type=submit],
.form-style-2 input[type=button]{
    border: none;
    padding: 8px 15px 8px 15px;
    background: #FF8500;
    color: #fff;
    box-shadow: 1px 1px 4px #DADADA;
    -moz-box-shadow: 1px 1px 4px #DADADA;
    -webkit-box-shadow: 1px 1px 4px #DADADA;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
}
.form-style-2 input[type=submit]:hover,
.form-style-2 input[type=button]:hover{
    background: #EA7B00;
    color: #fff;
}
</style>
  
  
</head>
<body data-vide-bg="Aadhee Birthday">
<script src="jquery.min.js"></script>
<div class="header_direction">
	INVITATION <button class="direction_btn" type="button" onClick="window.location='map.php'">DIRECTIONS</button>
</div>

<div id="ytplayer"></div>
<script>
var h = $(window).height();
var w = $(window).width();
h = h - 100;
$('#ytplayer').css({width:w+'px', height:h+'px', marginTop:'30px'});

// Load the IFrame Player API code asynchronously.
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
// Replace the 'ytplayer' element with an <iframe> and
// YouTube player after the API code downloads.
var player;
function onYouTubePlayerAPIReady() {
    player = new YT.Player('ytplayer', {
        height: h,
        width: w,
        videoId: '0O2YwTqDees',
        playerVars: {
            'autoplay': 1,
            'showinfo': 0,
            'controls': 1
        },
		events: {
			'onReady': function(e){
				
					player.playVideo(); 
				
			},
	    	'onStateChange': function(e){
				if (e.data === YT.PlayerState.ENDED) {
					player.playVideo(); 
				}
			}
	  }

    });
}

$( window ).resize(function() {
  var h = $(window).height();
var w = $(window).width();
h = h - 100;
$('#ytplayer').css({width:w+'px', height:h+'px', marginTop:'30px'});
});
</script>

<div class="footer_rsvp">
	<div class="left_column">
	RSVP
    <button class="confirm_btn" id="confirm" type="button" onClick="toggle_rsvp();">Yes, I am coming</button>
    </div>
    
    <div class="right_column">
	Sorry, I can't make it,
    <button class="confirm_btn" type="button" onclick="send_to_payment()">but I would like to buy him something</button>
    </div>
    <div class="clear"></div>
    
    <div class="rsvp_form" id="rsvp_form" align="center">
   
<div class="form-style-2" align="center">
<div class="form-style-2-heading">Provide your information</div>
 <form id="invite" name="invite" target="invite_iframe" method="post" enctype="multipart/form-data">

<label for="field1"><span>Name <span class="required">*</span></span><input class="input-field" type="text" id="user_name" name="user_name"></label>
<label for="field1"><span>Adult <span class="required">*</span></span>  
<button type="button" id="sub" class="sub">-</button>
    <input type="text" class="input-field_no" id="adult" name="adult" value="0" class="field" />
    <button type="button" id="add" class="add">+</button>
    </label>
    
<label for="field1"><span>Children <span class="required">*</span></span>
<button type="button" id="sub2" class="sub">-</button>
    <input class="input-field_no" type="text" id="children" name="children" value="0" class="field" />
    <button type="button" id="add2" class="add">+</button>

</label>
<div id="submit_status">
<label><span>&nbsp;</span><input type="submit" value="Submit" onClick="invite_submit()" /></label>
</div>
</form>
</div>

        </form>
        
    </div>
    
</div>

<?php
	echo '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" class="payPalForm" name="paypal_form">
			<input type="hidden" name="cmd" value="_donations" />
			<input type="hidden" name="item_name" value="ADEE DEV BIRTHDAY GIFT" />
			<input type="hidden" name="business" value="lalith@teamkaushik.com" />
			<input type="hidden" name="lc" value="UK" />
			<input type="hidden" name="currency_code" value="GBP" />
			<input type="hidden" name="amount" value="" />
		</form>';
		?>
<style>
.left_column{
	float:left;
	text-align:left;
}

.right_column{
	float:right;	
	text-align:right;
}

.left_column button, .right_column button{
	display:block;
	margin-top:5px;
}

.clear{
	clear:both;	
}
</style>

<script>


$('.add').click(function () {
    $(this).prev().val(+$(this).prev().val() + 1);
});
$('.sub').click(function () {
    if ($(this).next().val() > 0) $(this).next().val(+$(this).next().val() - 1);
});

function send_to_payment(){
	document.paypal_form.submit();
}

function toggle_rsvp()
{
	if($('#rsvp_form').hasClass('open')){
		$('#rsvp_form').removeClass('open');
		$("#rsvp_form").hide(1000);
		document.getElementById("confirm").innerHTML = "Yes, I am coming";
	}
	else{
		document.getElementById("confirm").innerHTML = "CLOSE";
		$("#rsvp_form").show();
		$('#rsvp_form').addClass('open');
	}
	
}


function invite_submit()
{
	var flag = 0;
	 
    if(document.getElementById('user_name').value=="")
	{
		document.getElementById('user_name').style.border="1px solid red";
		flag=1;
	}
	else
	{
		document.getElementById('user_name').style.borderColor="#CCCCCC";
	}
	
	
	
   
   if(flag==1)
	{
		alert("Please check the highlighted fields");
	}
	else
	{
		var vals = $('#invite').serialize();
		$('#submit_status').html('Sending... Please wait...');
		$.post('invite_email.php', vals, function(data){
			$('#submit_status').html(data);
			$('#invite')[0].reset();
			toggle_rsvp();
		});
	}
}


</script>

</body>
</html>