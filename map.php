<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Direction to Shelter Hotel</title>
<link rel="icon" type="image/png" href="images/favicon.png">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
<style>
body{
	padding:0px;
	margin:0px;	
}
</style>
</head>

<body>
<div id="map"></div>
<script src="jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKw1I9ZlI-piCBp2zXSuviBDVRjju-aYI"> </script> 
<script src="geoposition.js "></script> 
<script>
var directionsService, directionsDisplay, map, geocoder;
geocoder = new google.maps.Geocoder();
function initMap() {
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: {lat: 13.0326538, lng: 80.2654718}
  });
  directionsDisplay.setMap(map);

 
	if (geoPosition.init()) {
		geoPosition.getCurrentPosition(locationFound, defaultLocation, {enableHighAccuracy:true});
	}
	else{
		defaultLocation();
	}
    
 
}

function locationFound(position) {
	calculateAndDisplayRoute(directionsService, directionsDisplay, position.coords.latitude, position.coords.longitude);
}

function defaultLocation() {
	calculateAndDisplayRoute(directionsService, directionsDisplay, 13.0326538, 80.2654718);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay, lat, lng) {
  directionsService.route({
    origin: new google.maps.LatLng(lat,lng),
    destination: new google.maps.LatLng(13.032746324994596,80.26590027518546),
    travelMode: google.maps.TravelMode.DRIVING
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      var route = response.routes[0];
      
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

var h = $(window).height();
var w = $(window).width();
$('#map').css({width:w+'px', height:h+'px'});
google.maps.event.addDomListener(window, 'load', initMap);

</script>


</body>
</html>